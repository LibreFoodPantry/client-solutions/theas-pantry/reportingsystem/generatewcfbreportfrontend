# Generate WCFB Report Frontend 

## Project setup
```
yarn install
```
Make sure that the extention named Vetur is installed in Dev Container, then all .vue files should work.

### Start backend

1. `bin/backend-down.sh`
2. `bin/backend-up.sh` (this command should be modified since it does not start the rabbitmq service)
    - using the below command instead to start the backend

    ```bash
    docker-compose up -d
    ```

### Send fake data to backend

This dummy dataset is used to generate a report of September (09/01/2022 - 09/30/2022).

```
node ./src/send_Fakedata_RMQ/sendData.js 
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

Copyright © 2021 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.