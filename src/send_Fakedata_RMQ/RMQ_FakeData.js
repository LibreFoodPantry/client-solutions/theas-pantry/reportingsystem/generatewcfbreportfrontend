
class data {

    static guestData(){
        let guest1 = [{
            dateTime: '2022-06-20',
            student_id:'10101010',
            resident: true,
            zipCode: '20650',
            unemployement: true,
            assistance: {
                socSec: false,
                TANF: true,
                finAid: false,
                other: true,
                SNAP: true,
                WIC: false,
                breakfast: false,
                lunch: true,
                SFSP: false
            },
            household: [2, 21, 28, 60]
        },
        {
            dateTime: '2022-09-25',
            student_id:'10101010',
            resident: true,
            zipCode: '20650',
            unemployement: true,
            assistance: {
                socSec: false,
                TANF: true,
                finAid: false,
                other: true,
                SNAP: true,
                WIC: false,
                breakfast: false,
                lunch: true,
                SFSP: false
            },
            household: [2, 21, 28, 60]
        },
        {
            dateTime: '2022-09-20',
            student_id:'10101010',
            resident: true,
            zipCode: '20650',
            unemployement: true,
            assistance: {
                socSec: false,
                TANF: true,
                finAid: false,
                other: true,
                SNAP: true,
                WIC: false,
                breakfast: false,
                lunch: true,
                SFSP: false
            },
            household: [2, 21, 28, 60]
        },
        {
            dateTime: '2022-07-20',
            student_id:'10101011',
            resident: false,
            zipCode: '20650',
            unemployement: false,
            assistance: {
                socSec: false,
                TANF: false,
                finAid: false,
                other: false,
                SNAP: true,
                WIC: false,
                breakfast: false,
                lunch: false,
                SFSP: false
            },
            household: [1, 3, 21, 21, 68]
        },
        {
            dateTime: '2022-09-21',
            student_id:'10101011',
            resident: false,
            zipCode: '20650',
            unemployement: false,
            assistance: {
                socSec: false,
                TANF: false,
                finAid: false,
                other: false,
                SNAP: true,
                WIC: false,
                breakfast: false,
                lunch: false,
                SFSP: false
            },
            household: [1, 3, 21, 21, 68]
        },
        {
            dateTime: '2022-08-20',
            student_id:'10101012',
            resident: false,
            zipCode: '20650',
            unemployement: false,
            assistance: {
                socSec: true,
                TANF: true,
                finAid: true,
                other: false,
                SNAP: true,
                WIC: false,
                breakfast: true,
                lunch: false,
                SFSP: false
            },
            household: [1, 35, 50]
        },
        {
            dateTime: '2022-09-20',
            student_id:'10101012',
            resident: false,
            zipCode: '20650',
            unemployement: false,
            assistance: {
                socSec: true,
                TANF: true,
                finAid: true,
                other: false,
                SNAP: true,
                WIC: false,
                breakfast: true,
                lunch: false,
                SFSP: false
            },
            household: [1, 35, 50]
        },
        {
            dateTime: '2022-09-06',
            student_id:'10101013',
            resident: false,
            zipCode: '20653',
            unemployement: false,
            assistance: {
                socSec: true,
                TANF: false,
                finAid: false,
                other: false,
                SNAP: false,
                WIC: false,
                breakfast: true,
                lunch: false,
                SFSP: true
            },
            household: [1, 32, 49, 67]
        },
        {   
            dateTime: '2022-09-02',
            student_id:'10101014',
            resident: false,
            zipCode: '20654',
            unemployement: false,
            assistance: {
                socSec: true,
                TANF: false,
                finAid: false,
                other: false,
                SNAP: false,
                WIC: false,
                breakfast: true,
                lunch: false,
                SFSP: true
            },
            household: [1, 13, 16, 21, 45, 67]
        }];
        return guest1;
    }

    static inventoryData (){
        let inventory1 = [{
            student_id:'10101010',
            lbs: 1.2,
            dateTime: '2022-06-20' 
        },
        {
            student_id:'10101011',
            lbs: 2.6,
            dateTime: '2022-07-20' 
        },
        {
            student_id:'10101012',
            lbs: 3.8,
            dateTime: '2022-08-20' 
        }];

        return inventory1;
    }
}

module.exports = data;

