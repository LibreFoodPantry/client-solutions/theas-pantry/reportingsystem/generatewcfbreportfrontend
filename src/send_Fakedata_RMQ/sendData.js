var amqp = require('amqplib/callback_api');

const data = require('./RMQ_FakeData');

const inventoryData = data.inventoryData(); 

const guestData = data.guestData();

amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'inventory';

        channel.assertQueue(queue);
        channel.sendToQueue(queue, Buffer.from(JSON.stringify(inventoryData)));
        console.log(`message sent to ${queue}`);

    });
    setTimeout(function() {
        connection.close();
        process.exit(0)
        }, 500);

    
    //The channel for GuestInfor//
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'guestinfo';

        channel.assertQueue(queue);
        channel.sendToQueue(queue, Buffer.from(JSON.stringify(guestData)));
        console.log(`message sent to ${queue}`);

    });
    setTimeout(function() {
        connection.close();
        process.exit(0)
        }, 500);

    
});